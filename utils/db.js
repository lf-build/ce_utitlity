const MongoClient = require('mongodb').MongoClient;
var AdmZip = require('adm-zip');
const tools = require('./tools');
const mimetypes = process.env.MIME_TYPE || "text/xml";
const MaxDays = process.env.MAX_DAYS || 32;
const MaxDaysCibil = process.env.MAX_DAYS_CIBIL || 32;

// Perfios function to get Data
async function GetCollectionByDate(req, res, url, databaseName, collectionName, DateField, Fromdate, Todate) {
  var response = res;
  const key = Fromdate + "_To_" + Todate;
  MongoClient.connect(url, (err, client) => {
    if (err) {
      console.log("DB Connecection failed due to following Error : " + err);
    }
    const db = client.db(databaseName);
    var partFromdate = Fromdate.split('-');
    var partTodate = Todate.split('-');
    Fromdate = new Date(partFromdate[2] + '-' + partFromdate[1] + '-' + (partFromdate[0]) + "T18:30:59");
    Fromdate = new Date(Fromdate.setDate(Fromdate.getDate() - 1)); // minus one day.
    //Fromdate = new Date(Fromdate.getFullYear(), Fromdate.getMonth(), Fromdate.getDate(), 0, 0, 0).toUTCString();
    Todate = new Date(partTodate[2] + '-' + partTodate[1] + '-' + partTodate[0] + "T18:30:59");
    //Todate = new Date(Todate.getFullYear(), Todate.getMonth(), Todate.getDate(), 23, 59, 59).toUTCString();

    totalDays = Math.floor((Todate - Fromdate) / (1000 * 60 * 60 * 24));
    if (totalDays > MaxDays) {
      return res.render('index', { title: 'LF Extraction Utility', error: 'Max Date Range only Allow for :' + MaxDays + ' Days' });
    }
    console.log("Date Range from Date" + Fromdate + " To date " + Todate);
    db.collection(collectionName).aggregate([
      {
        $match: {
          $and: [{ "ProcessDate.DateTime": { $gt: Fromdate } },
          { "ProcessDate.DateTime": { $lt: Todate } }
          ]
        }
      }
    ]).toArray()
      .then((codeMap) => {
        console.log("Data found in Range:" + codeMap.length);
        if (codeMap.length > 0) {
          var zip = new AdmZip();
          codeMap.forEach(element => {
            if (typeof (element.XMLResponse) != "undefined" &&
              element.XMLResponse != null) {
              zip.addFile(element.EntityId + "_perfios_xmlResponse.xml", Buffer.alloc(element.XMLResponse.length, element.XMLResponse), "perfios request xml");
            }
            else {
              zip.addFile(element.EntityId + "_perfios_xmlResponse_Empty.xml", Buffer.alloc(0, ""), "perfios request xml");
            }
          });
          var willSendthis = zip.toBuffer();
          tools.download(willSendthis, mimetypes, response, collectionName, key);
          // client.close();
          // res.render('index', { title: 'LF Extraction Utility', error: '' })
        }
        else {
          client.close();
          res.render('index', { title: 'LF Extraction Utility', error: 'No Records Found....' });
        }
      });
  });
};
async function GetCollectionByAppNumber(req, res, url, databaseName, collectionName, DateField, EntityId) {
  var response = res;
  MongoClient.connect(url, (err, client) => {
    if (err) {
      console.log("DB Connecection failed due to following Error : " + err);
      res.render('index', { title: 'LF Extraction Utility', error: 'DB Connecection failed due to following Error : ' + err })
    }
    const db = client.db(databaseName);
    db.collection(collectionName).find({ "EntityId": EntityId }).toArray()
      .then((codeMap) => {
        if (typeof (codeMap) != "undefined" ||
          codeMap != null) {
          console.log(codeMap.length);
          if (codeMap.length > 0) {
            var zip = new AdmZip();
            codeMap.forEach(element => {

              if (typeof (element.XMLResponse) != "undefined" &&
                element.XMLResponse != null) {
                zip.addFile(element.EntityId + "_" + collectionName + "_xmlResponse.xml", Buffer.alloc(element.XMLResponse.length, element.XMLResponse), "request xml");
              }
              else {
                zip.addFile(element.EntityId + "_perfios_xmlResponse_Empty.xml", Buffer.alloc(0, ""), "perfios request xml");
              }
            });
            var willSendthis = zip.toBuffer();
            tools.download(willSendthis, mimetypes, response, collectionName, EntityId);
            client.close();
          }
          else {
            client.close();
            res.render('index', { title: 'LF Extraction Utility', error: 'No Records Found..' })
          }
        }
        else {
          client.close();
          res.render('index', { title: 'LF Extraction Utility', error: 'No Records Found..' })
        }
      });
  });
};

// Cibil fucntion to get data
async function CibilGetCollectionByDate(req, res, url, databaseName, collectionName, DateField, CibilFromdate, CibilTodate) {
  var response = res;
  const key = CibilFromdate + "_To_" + CibilTodate;
  MongoClient.connect(url, (err, client) => {
    if (err) {
      console.log("DB Connecection failed due to following Error : " + err);
    }
    const db = client.db(databaseName);
    var partFromdate = CibilFromdate.split('-');
    var partTodate = CibilTodate.split('-');
    CibilFromdate = new Date(partFromdate[2] + '-' + partFromdate[1] + '-' + (partFromdate[0]) + "T18:30:59");
    CibilFromdate = new Date(CibilFromdate.setDate(CibilFromdate.getDate() - 1)); // minus one day.
    //Fromdate = new Date(Fromdate.getFullYear(), Fromdate.getMonth(), Fromdate.getDate(), 0, 0, 0).toUTCString();
    CibilTodate = new Date(partTodate[2] + '-' + partTodate[1] + '-' + partTodate[0] + "T18:30:59");
    //Todate = new Date(Todate.getFullYear(), Todate.getMonth(), Todate.getDate(), 23, 59, 59).toUTCString();

    totalDays = Math.floor((CibilTodate - CibilFromdate) / (1000 * 60 * 60 * 24));
    if (totalDays > MaxDaysCibil) {
      return res.render('cibilreport', { title: 'Extract Cibil XML using :', error: 'Max Date Range only Allow for :' + MaxDaysCibil + ' Days' });
    }
    console.log("Date Range from Date" + CibilFromdate + " To date " + CibilTodate);
    db.collection(collectionName).aggregate([
      {
        $match: {
          $and: [{ "ReportDate.DateTime": { $gt: CibilFromdate } },
          { "ReportDate.DateTime": { $lt: CibilTodate } }
          ]
        }
      }
    ]).toArray()
      .then((codeMap) => {
        console.log("Data found in Range:" + codeMap.length);
        if (codeMap.length > 0) {
          var zip = new AdmZip();
          codeMap.forEach(function (element, i) {
            if (typeof (element.RequestXML) != "undefined" &&
              element.RequestXML != null) {
              zip.addFile(element.EntityId + "_" + (i + 1) + "_cibil_xmlRequest.xml", Buffer.alloc(element.RequestXML.length, element.RequestXML), "cibil request xml");
            }
            else {
              zip.addFile(element.EntityId + "_cibilData_" + (i + 1) + "xmlRequest_Empty.xml", Buffer.alloc(0, ""), "cibil request xml");
            }

            if (typeof (element.ResponseXML) != "undefined" &&
              element.ResponseXML != null) {
              zip.addFile(element.EntityId + "_" + (i + 1) + "_cibil_xmlResponse.xml", Buffer.alloc(element.ResponseXML.length, element.ResponseXML), "cibil response xml");
            }
            else {
              zip.addFile(element.EntityId + "_cibilData_" + (i + 1) + "xmlResponse_Empty.xml", Buffer.alloc(0, ""), "cibil response xml");
            }

            if (typeof (element.BureauResponseRaw) != "undefined" &&
              element.BureauResponseRaw != null && element.BureauResponseRaw.length > 0) {
              zip.addFile(element.EntityId + "_" + (i + 1) + "_cibil_CIBILRawText.xml", Buffer.alloc(element.BureauResponseRaw.length, element.BureauResponseRaw), "CIBILRawText xml");
            }
            else {
              zip.addFile(element.EntityId + "_cibilData_" + (i + 1) + "CIBILRawText_Empty.xml", Buffer.alloc(0, ""), "CIBILRawText xml");
            }

          });
          var willSendthis = zip.toBuffer();
          tools.download(willSendthis, mimetypes, response, collectionName, key);
          // client.close();
          // res.render('index', { title: 'LF Extraction Utility', error: '' })
        }
        else {
          client.close();
          res.render('cibilreport', { title: 'Extract CIBIL XML using :', error: 'No Records Found....' })
        }
      });
  });
};
async function CibilGetCollectionByAppNumber(req, res, url, databaseName, collectionName, DateField, EntityId) {
  var response = res;
  MongoClient.connect(url, (err, client) => {
    if (err) {
      console.log("DB Connecection failed due to following Error : " + err);
      res.render('cibilreport', { title: 'Extract Cibil XML using :', error: 'DB Connecection failed due to following Error : ' + err })
    }
    const db = client.db(databaseName);
    db.collection(collectionName).find({ "EntityId": EntityId }).toArray()
      .then((codeMap) => {
        if (typeof (codeMap) != "undefined" ||
          codeMap != null) {
          console.log(codeMap.length);
          if (codeMap.length > 0) {
            var zip = new AdmZip();
            codeMap.forEach(function (element, i) {
              // Reques XML
              if (typeof (element.RequestXML) != "undefined" &&
                element.RequestXML != null) {
                zip.addFile(element.EntityId + "_" + collectionName + "_" + (i + 1) + "_xmlRequest.xml", Buffer.alloc(element.RequestXML.length, element.RequestXML), "request xml");
              }
              else {
                zip.addFile(element.EntityId + "_cibilData_" + (i + 1) + "xmlRequest_Empty.xml", Buffer.alloc(0, ""), "Cibil request xml");
              }
              // Response XML
              if (typeof (element.ResponseXML) != "undefined" &&
                element.ResponseXML != null) {
                zip.addFile(element.EntityId + "_" + collectionName + "_" + (i + 1) + "_xmlResponse.xml", Buffer.alloc(element.ResponseXML.length, element.ResponseXML), "response xml");
              }
              else {
                zip.addFile(element.EntityId + "_cibilData_" + (i + 1) + "_xmlResponse_Empty.xml", Buffer.alloc(0, ""), "Cibil response xml");
              }
              // Raw BureauResponse 
              if (typeof (element.BureauResponseRaw) != "undefined" &&
                element.BureauResponseRaw != null && element.BureauResponseRaw.length > 0) {
                zip.addFile(element.EntityId + "_" + collectionName + "_" + (i + 1) + "_CIBILRawText.xml", Buffer.alloc(element.BureauResponseRaw.length, element.BureauResponseRaw), "CIBILRawText xml");
              }
              else {
                zip.addFile(element.EntityId + "_cibilData_" + (i + 1) + "_CIBILRawText_Empty.xml", Buffer.alloc(0, ""), "Cibil raw text xml");
              }


            });
            var willSendthis = zip.toBuffer();
            tools.download(willSendthis, mimetypes, response, collectionName, EntityId);
            client.close();
          }
          else {
            client.close();
            res.render('cibilreport', { title: 'Extract Cibil XML using :', error: 'No Records Found..' })
          }
        }
        else {
          client.close();
          res.render('cibilreport', { title: 'Extract Cibil XML using :', error: 'No Records Found..' })
        }
      });
  });
};
module.exports.GetCollectionByDate = GetCollectionByDate;
module.exports.GetCollectionByAppNumber = GetCollectionByAppNumber;

module.exports.CibilGetCollectionByDate = CibilGetCollectionByDate;
module.exports.CibilGetCollectionByAppNumber = CibilGetCollectionByAppNumber;