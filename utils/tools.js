exports.download = function (element, mimetype, res, collectionName, key) {
    res.writeHead(200, {
        'Content-Type': mimetype,
        'Content-disposition': 'attachment;filename=' + collectionName + '_' + key + "_" + (new Date).getTime() + ".zip"
    });
    res.end(new Buffer(element), 'binary');
};