var express = require('express');
var router = express.Router();
const DbCollection = require('../utils/db');
const url = process.env.MONGO_URL || "mongodb://10.101.0.29:27017";
/* GET home page. */
router.get('Cibi', function (req, res, next) {
  res.render('index', { title: 'Extract CIBIL XML using :', error: '' });
});
// Cibil Extraction Utility POST ACTION
router.post('*', function (req, res, next) {
  //No errors were found.  Passed Validation!
  if (typeof (req.body.fromdate) != "undefined" && typeof (req.body.todate) != "undefined") {
    var CibilReprotDate = {
      fromdate: req.body.fromdate.replace('/g', '-'),
      todate: req.body.todate.replace('/g', '-'),
    }
    DbCollection.GetCollectionByDate(req, res, url, "cibilData", "cibilData", "ProcessDate", CibilReprotDate.fromdate, CibilReprotDate.todate);
  }
  else if (typeof (req.body.entityId) != "undefined") {
    var CibilReprotDate = {
      applicationNumber: req.body.entityId.replace('/g', '-')
    }
    DbCollection.GetCollectionByAppNumber(req, res, url, "cibilData", "cibilData", "EntityId", CibilReprotDate.applicationNumber);
  }
})
module.exports = router;
