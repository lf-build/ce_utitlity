var express = require('express');
var router = express.Router();
const DbCollection = require('../utils/db');
const Utility = require('../utils/minifi');
var cleaner = require('clean-html');
const url = process.env.MONGO_URL || "mongodb://10.101.0.29:27017";
/* GET home page. */
router.get('/minifi', function (req, res, next) {
  res.render('allInOne', { beauty: '', result: '', title: '', error: '' });
});

// Perfios Extraction Utility POST ACTION
router.post('/minifi', function (req, res, next) {
  if (req.body.textminifi) {
    res.render('allInOne', { beauty: '', result: Utility.minify(req.body.userContent), title: '', error: '' });
  }
  else if (req.body.htmlBeautify) {
    res.render('allInOne', { result: '', beauty: Utility.beautify(req.body.userContent), title: '', error: '' });
  }
  else if (req.body.jsonBeautify) {
    res.render('allInOne', { result: '', beauty: Utility.beautifyJSON(req.body.userContent), title: '', error: '' });
  }
  else if (req.body.removeSpl) {
    res.render('allInOne', { result: '', beauty: Utility.beautify(Utility.removeSpl(req.body.userContent)), title: '', error: '' });
  }
  else {
    res.render('allInOne', { result: '', beauty: '', title: '', error: 'All Clear' });
  }
})




router.get('/', function (req, res, next) {
  res.render('index', { title: 'Extract Perfios XML', error: '' });
});














router.get('/report', function (req, res, next) {
  res.render('cibilreport', { title: 'Extract Cibil XML', error: '' });
});

// Perfios Extraction Utility POST ACTION
router.post('/', function (req, res, next) {
  //No errors were found.  Passed Validation!
  if (typeof (req.body.fromdate) != "undefined" && typeof (req.body.todate) != "undefined") {
    var PerfiosReprotDate = {
      fromdate: req.body.fromdate.replace('/g', '-'),
      todate: req.body.todate.replace('/g', '-'),
    }
    DbCollection.GetCollectionByDate(req, res, url, "perfios-Data", "perfios-data", "ProcessDate", PerfiosReprotDate.fromdate, PerfiosReprotDate.todate);
  }
  else if (typeof (req.body.entityId) != "undefined") {
    var PerfiosReprotDate = {
      applicationNumber: req.body.entityId.replace('/g', '-')
    }
    DbCollection.GetCollectionByAppNumber(req, res, url, "perfios-Data", "perfios-data", "EntityId", PerfiosReprotDate.applicationNumber);
  }
})

// Cibil Extraction Utility POST ACTION
router.post('/report', function (req, res, next) {
  //No errors were found.  Passed Validation!
  if (typeof (req.body.cibilfromdate) != "undefined" && typeof (req.body.cibiltodate) != "undefined") {
    var CibilReprotDate = {
      cibilfromdate: req.body.cibilfromdate.replace('/g', '-'),
      cibiltodate: req.body.cibiltodate.replace('/g', '-'),
    }
    DbCollection.CibilGetCollectionByDate(req, res, url, "cibilData", "cibilData", "ProcessDate", CibilReprotDate.cibilfromdate, CibilReprotDate.cibiltodate);
  }
  else if (typeof (req.body.entityId) != "undefined") {
    var CibilReprotDate = {
      applicationNumber: req.body.entityId.replace('/g', '-')
    }
    DbCollection.CibilGetCollectionByAppNumber(req, res, url, "cibilData", "cibilData", "EntityId", CibilReprotDate.applicationNumber);
  }
})
module.exports = router;
